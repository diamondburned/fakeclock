package main

import (
	"errors"
	"os"
	"strconv"
	"time"

	"github.com/beevik/ntp"
)

var (
	t int64

	tt time.Time

	ntpServer = "3.arch.pool.ntp.org"

	cl, _ = os.OpenFile("./clock.tmp", os.O_RDWR|os.O_CREATE|os.O_SYNC, os.ModePerm)

	errDateNotFound = errors.New("Date not found")

	err error

	writeCh = make(chan struct{}, 1)
)

func main() {
	defer cl.Close()

	done := make(chan struct{}, 1)

	tickrate, _ := time.ParseDuration("1s")
	throttle := time.Tick(tickrate)

	tt, err = ntp.Time(ntpServer)
	if err != nil {
		panic(err)
	} else {
		t = tt.Unix()
	}

	for {
		<-throttle

		if (t % 5) == 0 {
			done <- struct{}{}
			go func() {
				tt, err = ntp.Time(ntpServer)
				if err != nil {
					t = t + 1
				} else {
					t = tt.Unix()
				}

				<-done
			}()
		} else {
			t = t + 1
		}

		writeCh <- struct{}{}
		go write()
	}
}

func write() {
	<-writeCh
	cl.Truncate(0)
	cl.WriteString(strconv.FormatInt(t, 10))
}

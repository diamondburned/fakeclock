#!/bin/bash
enable -f sleep
clock="./clock.tmp"

function update() {
	dateFromServer=$(echo $(curl -v --silent https://google.com/ 2>&1 | grep -i -F '< date' | cut -d':' -f2-))
	/bin/date -d "$dateFromServer" +%s > "$clock"
}

while :; do
	[[ $(printf "%(%S)T") = *"0" ]] && {
		update &
	} || {
		date=$(< "$clock")
		echo $(( date + 1 )) > "$clock"
	}
	
	# sleep
	read -r -t 1 <&1 3<&- 3<&0 <&3
done

